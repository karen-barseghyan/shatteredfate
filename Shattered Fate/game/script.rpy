﻿init python:
    def update_player_hp_sprite(player_health, player_max_health):
        player_health_percentage = player_health / player_max_health
        if player_health_percentage >= 1:
            renpy.store.Player_HP_Sprite = "HP_100"
        elif player_health_percentage >= 0.95:
            renpy.store.Player_HP_Sprite = "HP_95"
        elif player_health_percentage >= 0.90:
            renpy.store.Player_HP_Sprite = "HP_90"
        elif player_health_percentage >= 0.85:
            renpy.store.Player_HP_Sprite = "HP_85"
        elif player_health_percentage >= 0.80:
            renpy.store.Player_HP_Sprite = "HP_80"
        elif player_health_percentage >= 0.75:
            renpy.store.Player_HP_Sprite = "HP_75"
        elif player_health_percentage >= 0.70:
            renpy.store.Player_HP_Sprite = "HP_70"
        elif player_health_percentage >= 0.65:
            renpy.store.Player_HP_Sprite = "HP_65"
        elif player_health_percentage >= 0.60:
            renpy.store.Player_HP_Sprite = "HP_60"
        elif player_health_percentage >= 0.55:
            renpy.store.Player_HP_Sprite = "HP_55"
        elif player_health_percentage >= 0.50:
            renpy.store.Player_HP_Sprite = "HP_50"
        elif player_health_percentage >= 0.45:
            renpy.store.Player_HP_Sprite = "HP_45"
        elif player_health_percentage >= 0.40:
            renpy.store.Player_HP_Sprite = "HP_40"
        elif player_health_percentage >= 0.35:
            renpy.store.Player_HP_Sprite = "HP_35"
        elif player_health_percentage >= 0.30:
            renpy.store.Player_HP_Sprite = "HP_30"
        elif player_health_percentage >= 0.25:
            renpy.store.Player_HP_Sprite = "HP_25"
        elif player_health_percentage >= 0.20:
            renpy.store.Player_HP_Sprite = "HP_20"
        elif player_health_percentage >= 0.15:
            renpy.store.Player_HP_Sprite = "HP_15"
        elif player_health_percentage >= 0.10:
            renpy.store.Player_HP_Sprite = "HP_10"
        elif player_health_percentage > 0.00:
            renpy.store.Player_HP_Sprite = "HP_05"
        else:
            renpy.store.Player_HP_Sprite = "HP_00"

    def update_player_ap_sprite(player_action_points, player_max_action_points):
        player_action_percentage = player_action_points / player_max_action_points
        
        if player_action_percentage >= 1:
            renpy.store.Player_AP_Sprite = "AP_100"
        elif player_action_percentage >= 0.95:
            renpy.store.Player_AP_Sprite = "AP_95"
        elif player_action_percentage >= 0.90:
            renpy.store.Player_AP_Sprite = "AP_90"
        elif player_action_percentage >= 0.85:
            renpy.store.Player_AP_Sprite = "AP_85"
        elif player_action_percentage >= 0.80:
            renpy.store.Player_AP_Sprite = "AP_80"
        elif player_action_percentage >= 0.75:
            renpy.store.Player_AP_Sprite = "AP_75"
        elif player_action_percentage >= 0.70:
            renpy.store.Player_AP_Sprite = "AP_70"
        elif player_action_percentage >= 0.65:
            renpy.store.Player_AP_Sprite = "AP_65"
        elif player_action_percentage >= 0.60:
            renpy.store.Player_AP_Sprite = "AP_60"
        elif player_action_percentage >= 0.55:
            renpy.store.Player_AP_Sprite = "AP_55"
        elif player_action_percentage >= 0.50:
            renpy.store.Player_AP_Sprite = "AP_50"
        elif player_action_percentage >= 0.45:
            renpy.store.Player_AP_Sprite = "AP_45"
        elif player_action_percentage >= 0.40:
            renpy.store.Player_AP_Sprite = "AP_40"
        elif player_action_percentage >= 0.35:
            renpy.store.Player_AP_Sprite = "AP_35"
        elif player_action_percentage >= 0.30:
            renpy.store.Player_AP_Sprite = "AP_30"
        elif player_action_percentage >= 0.25:
            renpy.store.Player_AP_Sprite = "AP_25"
        elif player_action_percentage >= 0.20:
            renpy.store.Player_AP_Sprite = "AP_20"
        elif player_action_percentage >= 0.15:
            renpy.store.Player_AP_Sprite = "AP_15"
        elif player_action_percentage >= 0.10:
            renpy.store.Player_AP_Sprite = "AP_10"
        elif player_action_percentage > 0.00:
            renpy.store.Player_AP_Sprite = "AP_05"
        else:
            renpy.store.Player_AP_Sprite = "AP_00"

    def update_enemy_health_sprite(enemy_hp, max_enemy_hp):
        enemy_health_percentage = enemy_hp / max_enemy_hp
        
        if enemy_health_percentage >= 1:
            renpy.store.Enemy_Health_Sprite = "Enemy_100"
        elif enemy_health_percentage >= 0.95:
            renpy.store.Enemy_Health_Sprite = "Enemy_95"
        elif enemy_health_percentage >= 0.90:
            renpy.store.Enemy_Health_Sprite = "Enemy_90"
        elif enemy_health_percentage >= 0.85:
            renpy.store.Enemy_Health_Sprite = "Enemy_85"
        elif enemy_health_percentage >= 0.80:
            renpy.store.Enemy_Health_Sprite = "Enemy_80"
        elif enemy_health_percentage >= 0.75:
            renpy.store.Enemy_Health_Sprite = "Enemy_75"
        elif enemy_health_percentage >= 0.70:
            renpy.store.Enemy_Health_Sprite = "Enemy_70"
        elif enemy_health_percentage >= 0.65:
            renpy.store.Enemy_Health_Sprite = "Enemy_65"
        elif enemy_health_percentage >= 0.60:
            renpy.store.Enemy_Health_Sprite = "Enemy_60"
        elif enemy_health_percentage >= 0.55:
            renpy.store.Enemy_Health_Sprite = "Enemy_55"
        elif enemy_health_percentage >= 0.50:
            renpy.store.Enemy_Health_Sprite = "Enemy_50"
        elif enemy_health_percentage >= 0.45:
            renpy.store.Enemy_Health_Sprite = "Enemy_45"
        elif enemy_health_percentage >= 0.40:
            renpy.store.Enemy_Health_Sprite = "Enemy_40"
        elif enemy_health_percentage >= 0.35:
            renpy.store.Enemy_Health_Sprite = "Enemy_35"
        elif enemy_health_percentage >= 0.30:
            renpy.store.Enemy_Health_Sprite = "Enemy_30"
        elif enemy_health_percentage >= 0.25:
            renpy.store.Enemy_Health_Sprite = "Enemy_25"
        elif enemy_health_percentage >= 0.20:
            renpy.store.Enemy_Health_Sprite = "Enemy_20"
        elif enemy_health_percentage >= 0.15:
            renpy.store.Enemy_Health_Sprite = "Enemy_15"
        elif enemy_health_percentage >= 0.10:
            renpy.store.Enemy_Health_Sprite = "Enemy_10"
        elif enemy_health_percentage > 0.00:
            renpy.store.Enemy_Health_Sprite = "Enemy_05"
        else:
            renpy.store.Enemy_Health_Sprite = "Enemy_00"


#Player variables
default player_class = None
default player_name = None
default player_action_points = 100
default player_max_action_points = 100
default player_health = 100
default player_max_health = 100
default player_min_damage = 20
default player_max_damage = 40
default player_special_damage_multiplier = 2.00
default player_special_bonus_effect_chance = 10
default player_special_attack_cost = 25
default is_player_defending = False

#Upgrades variables
default health_increase_per_level = 50
default action_points_per_level = 50
default damage_increase_per_level = 10
default special_damage_multiplier_increase_per_level = 0.2
default special_bonus_effect_chance_per_level = 10

#Inventory variables
default health_potions_amount = 1
default action_points_potions_amount = 1
default smoke_bombs_amount = 1

#enemy variables
default base_enemy_hp = 100
default max_enemy_hp = 100
default enemy_hp = 100
default last_enemy = None
default base_enemy_min_damage = 10
default base_enemy_max_damage = 20
default enemy_min_damage = 10
default enemy_max_damage = 20
default negotiate_dc = 10 #The hgiher DC the harder it is to negotiate, 21 DC makes it impossible.
default run_dc = 10 #The higher DC the harder it is to run, 21 DC makes it impossible.
default enemies_defeated = 0
default enemy_hp_increase_per_level = 50
default enemy_damage_inrease_per_level = 10
default enemy_boss_stats_multiplier = 2
default enemies_required_for_boss = 10
default is_enemy_paralyzed = False

#map variables
default last_map = None

# name of the character.
define sk = Character("Skeleton King")
define enemy = Character("Enemy")

# Npc images.
image SkeletonKing_Character = "Images/Npcs/SkeletonKing_Character.png"
image SkeletonKing_Background = "Images/Npcs/SkeletonKing_Background.png"

# Enemy images.
image Enemy_Sprite = "Images/Enemies/Bandit_Character.png"
image Bandit_Character = "Images/Enemies/Bandit_Character.png"
image Bandit_Background = "Images/Enemies/Bandit_Background.png"
image Bandit_Boss = "Images/Enemies/Bandit_Bloodied.png"
image Cthulhu_Character = "Images/Enemies/Cthulhu_Character.png"
image Cthulhu_Background = "Images/Enemies/Cthulhu_Background.png"
image Cthulhu_Boss = "Images/Enemies/Cthulhu_Bloodied.png"
image Demon_Character = "Images/Enemies/Demon_Character.png"
image Demon_Background = "Images/Enemies/Demon_Background.png"
image Demon_Boss = "Images/Enemies/Demon_Bloodied.png"
image Gargoyle_Character = "Images/Enemies/Gargoyle_Character.png"
image Gargoyle_Background = "Images/Enemies/Gargoyle_Background.png"
image Gargoyle_Boss = "Images/Enemies/Gargoyle_Bloodied.png"
image Slime_Character = "Images/Enemies/Slime_Character.png"
image Slime_Background = "Images/Enemies/Slime_Background.png"
image Slime_Boss = "Images/Enemies/Slime_Bloodied.png"
image Wolf_Character = "Images/Enemies/Wolf_Character.png"
image Wolf_Background = "Images/Enemies/Wolf_Background.png"
image Wolf_Boss = "Images/Enemies/Wolf_Bloodied.png"

# Enemy attacks.
image Bandit_Attack = "Images/Enemies/Bandit_Attack.png"
image Cthulhu_Attack = "Images/Enemies/Cthulhu_Attack.png"
image Demon_Attack = "Images/Enemies/Demon_Attack.png"
image Gargoyle_Attack = "Images/Enemies/Gargoyle_Attack.png"
image Slime_Attack = "Images/Enemies/Slime_Attack.png"
image Wolf_Attack = "Images/Enemies/Wolf_Attack.png"
image Boss_Attack = "Images/Enemies/Z_Bloodied_Attack.png"

# Classes images.
image Artificer_Character = "Images/Classes/Artificer_Character.png"
image Artificer_Background = "Images/Classes/Artificer_Background.png"
image Assassin_Character = "Images/Classes/Assassin_Character.png"
image Assassin_Background = "Images/Classes/Assassin_Background.png"
image Berserker_Character = "Images/Classes/Berserker_Character.png"
image Berserker_Background = "Images/Classes/Berserker_Background.png"
image Gunner_Character = "Images/Classes/Gunner_Character.png"
image Gunner_Background = "Images/Classes/Gunner_Background.png"
image Monk_Character = "Images/Classes/Monk_Character.png"
image Monk_Background = "Images/Classes/Monk_Background.png"
image Pirate_Character = "Images/Classes/Pirate_Character.png"
image Pirate_Background = "Images/Classes/Pirate_Background.png"

# Player attack images.
image Artificer_Primary_Attack = "Images/Classes/Artificer_Attack_1.png"
image Assassin_Primary_Attack = "Images/Classes/Assassin_Attack_1.png"
image Berserker_Primary_Attack = "Images/Classes/Berserker_Attack_1.png"
image Gunner_Primary_Attack = "Images/Classes/Gunner_Attack_1.png"
image Monk_Primary_Attack = "Images/Classes/Monk_Attack_1.png"
image Pirate_Primary_Attack = "Images/Classes/Pirate_Attack_1.png"

# Item images
image HP_Potion = "Images/Items/HealthPotion.png"
image AP_Potion = "Images/Items/ActionPointsPotion.png"
image Smoke_Bomb = "Images/Items/SmokeBomb.png"

# Ability images
image Defend = "Images/Abilities/Defend.png"

# Scenes:
image ShatteredTitleScreen = "Images/Menu/TitleScreenShattered.png"
image AbandonedCastle = "Images/Maps/AbandonedCastle.png"
image AncientRuins = "Images/Maps/AncientRuins.png"
image CityInTheClouds = "Images/Maps/CityInTheClouds.png"
image CrystalCaverns = "Images/Maps/CrystalCaverns.png"
image DarkForest = "Images/Maps/DarkForest.png"
image EnchantedGarden = "Images/Maps/EnchantedGarden.png"
image FloatingIslands = "Images/Maps/FloatingIslands.png"
image GrassyFields = "Images/Maps/GrassyFields.png"
image HallOfMirrors = "Images/Maps/HallOfMirrors.png"
image HauntedMansion = "Images/Maps/HauntedMansion.png"
image HiddenLaboratory = "Images/Maps/HiddenLaboratory.png"
image LavaFields = "Images/Maps/LavaFields.png"
image MountainRange = "Images/Maps/MountainRange.png"
image PirateCove = "Images/Maps/PirateCove.png"
image ScorchingDesert = "Images/Maps/ScorchingDesert.png"
image SnowyMountains = "Images/Maps/SnowyMountains.png"
image SwampyMarsh = "Images/Maps/SwampyMarsh.png"
image Graveyard = "Images/Maps/Graveyard.png"

# Bars: 
image HP_100 = "Images/Bars/HP_100.png"
image HP_95 = "Images/Bars/HP_95.png"
image HP_90 = "Images/Bars/HP_90.png"
image HP_85 = "Images/Bars/HP_85.png"
image HP_80 = "Images/Bars/HP_80.png"
image HP_75 = "Images/Bars/HP_75.png"
image HP_70 = "Images/Bars/HP_70.png"
image HP_65 = "Images/Bars/HP_65.png"
image HP_60 = "Images/Bars/HP_60.png"
image HP_55 = "Images/Bars/HP_55.png"
image HP_50 = "Images/Bars/HP_50.png"
image HP_45 = "Images/Bars/HP_45.png"
image HP_40 = "Images/Bars/HP_40.png"
image HP_35 = "Images/Bars/HP_35.png"
image HP_30 = "Images/Bars/HP_30.png"
image HP_25 = "Images/Bars/HP_25.png"
image HP_20 = "Images/Bars/HP_20.png"
image HP_15 = "Images/Bars/HP_15.png"
image HP_10 = "Images/Bars/HP_10.png"
image HP_05 = "Images/Bars/HP_05.png"
image HP_00 = "Images/Bars/HP_00.png"

image AP_100 = "Images/Bars/AP_100.png"
image AP_95 = "Images/Bars/AP_95.png"
image AP_90 = "Images/Bars/AP_90.png"
image AP_85 = "Images/Bars/AP_85.png"
image AP_80 = "Images/Bars/AP_80.png"
image AP_75 = "Images/Bars/AP_75.png"
image AP_70 = "Images/Bars/AP_70.png"
image AP_65 = "Images/Bars/AP_65.png"
image AP_60 = "Images/Bars/AP_60.png"
image AP_55 = "Images/Bars/AP_55.png"
image AP_50 = "Images/Bars/AP_50.png"
image AP_45 = "Images/Bars/AP_45.png"
image AP_40 = "Images/Bars/AP_40.png"
image AP_35 = "Images/Bars/AP_35.png"
image AP_30 = "Images/Bars/AP_30.png"
image AP_25 = "Images/Bars/AP_25.png"
image AP_20 = "Images/Bars/AP_20.png"
image AP_15 = "Images/Bars/AP_15.png"
image AP_10 = "Images/Bars/AP_10.png"
image AP_05 = "Images/Bars/AP_05.png"
image AP_00 = "Images/Bars/AP_00.png"

image Enemy_100 = "Images/Bars/Enemy_100.png"
image Enemy_95 = "Images/Bars/Enemy_95.png"
image Enemy_90 = "Images/Bars/Enemy_90.png"
image Enemy_85 = "Images/Bars/Enemy_85.png"
image Enemy_80 = "Images/Bars/Enemy_80.png"
image Enemy_75 = "Images/Bars/Enemy_75.png"
image Enemy_70 = "Images/Bars/Enemy_70.png"
image Enemy_65 = "Images/Bars/Enemy_65.png"
image Enemy_60 = "Images/Bars/Enemy_60.png"
image Enemy_55 = "Images/Bars/Enemy_55.png"
image Enemy_50 = "Images/Bars/Enemy_50.png"
image Enemy_45 = "Images/Bars/Enemy_45.png"
image Enemy_40 = "Images/Bars/Enemy_40.png"
image Enemy_35 = "Images/Bars/Enemy_35.png"
image Enemy_30 = "Images/Bars/Enemy_30.png"
image Enemy_25 = "Images/Bars/Enemy_25.png"
image Enemy_20 = "Images/Bars/Enemy_20.png"
image Enemy_15 = "Images/Bars/Enemy_15.png"
image Enemy_10 = "Images/Bars/Enemy_10.png"
image Enemy_05 = "Images/Bars/Enemy_05.png"
image Enemy_00 = "Images/Bars/Enemy_00.png"

image DMG_Background = "Images/Bars/DMG_Background.png"
image AP_Background = "Images/Bars/DMG_Background.png"
image ENEMY_Background = "Images/Bars/DMG_Background.png"

style damage_text:
    size 30
    outlines [(10, "#000000", 0, 0)]
    font "medieval.ttf"
    xalign 0
    yalign 0.5

# Effects
transform zoom_in:
    subpixel True
    zoom 2 alpha 1
    xalign 0.5 yalign 0.5
    easein 0.25 zoom 1.00 alpha 1.00

transform enemy_appear:
    subpixel True
    zoom 2 alpha 0
    xalign 0.5 yalign 0.5
    matrixcolor BrightnessMatrix (0.5)
    easein 0.25 zoom 1.00 alpha 1.00 matrixcolor BrightnessMatrix (0)

transform enemy_run_away:
    subpixel True
    zoom 1 alpha 1
    xalign 0.5 yalign 0.5
    easeout 0.5 zoom 0.0 alpha 0.0

transform zoom_in_right:
    subpixel True
    zoom 1.5 alpha 1.0
    xalign 1.0 yalign 1.0
    easein 0.5 zoom 1.00 alpha 1.00

transform player_ready_to_fight:
    subpixel True
    zoom 1.5 alpha 1.0
    xalign 0.0 yalign 1.0
    matrixcolor BrightnessMatrix (0.5)
    easein 0.5 zoom 1.00 alpha 1.00 matrixcolor BrightnessMatrix (0)

transform player_hp_show:
    subpixel True
    zoom 1.5 alpha 0.9
    xalign 0.2 yalign 0.05
    matrixcolor BrightnessMatrix (0.5)
    easein 0.5 zoom 1.00 matrixcolor BrightnessMatrix (0) 

transform player_ap_show:
    subpixel True
    zoom 1.5 alpha 0.9
    xalign 0.2 yalign 0.125
    matrixcolor BrightnessMatrix (0.5)
    easein 0.5 zoom 1.00 matrixcolor BrightnessMatrix (0) 

transform enemy_hp_show:
    subpixel True
    zoom 1.5 alpha 0.9
    xalign 0.8 yalign 0.085
    matrixcolor BrightnessMatrix (0.5)
    easein 0.5 zoom 1.00 matrixcolor BrightnessMatrix (0) 

transform showcase_thing_left:
    subpixel True
    zoom 1.5 alpha 1.0
    xalign 0.0 yalign 0.5
    matrixcolor BrightnessMatrix (0.5)
    easein 0.5 zoom 1.00 alpha 1.00 matrixcolor BrightnessMatrix (0)

transform showcase_thing_right:
    subpixel True
    zoom 1.5 alpha 1.0
    xalign 1.0 yalign 0.5
    matrixcolor BrightnessMatrix (0.5)
    easein 0.5 zoom 1.00 alpha 1.00 matrixcolor BrightnessMatrix (0)

transform enemy_ready_to_fight:
    subpixel True
    zoom 1.5 alpha 1.0
    xalign 1.0 yalign 1.0
    matrixcolor BrightnessMatrix (0.5)
    easein 0.5 zoom 1.00 alpha 1.00 matrixcolor BrightnessMatrix (0)

transform enemy_dead:
    subpixel True
    alpha 5.0
    matrixcolor BrightnessMatrix (0.5)
    easein 2 alpha 0.00 matrixcolor BrightnessMatrix (-1.00) yalign -2

transform flash:
    subpixel True
    matrixcolor BrightnessMatrix (0.5)
    easein 0.25 matrixcolor BrightnessMatrix (0)

transform reverse_flash:
    subpixel True
    matrixcolor BrightnessMatrix (-1)
    easein 0.25 matrixcolor BrightnessMatrix (0)

transform flash_right:
    subpixel True
    matrixcolor BrightnessMatrix (0.75)
    xalign 1.0 yalign 1.0
    easein 0.5 matrixcolor BrightnessMatrix (0)

transform take_damage:
    subpixel True
    matrixcolor TintMatrix("#f00")
    easein 0.5 matrixcolor TintMatrix("#fff")

transform take_damage_text:
    subpixel True
    matrixcolor TintMatrix("#fff")  yalign 0.065 alpha 1.0 xalign 0.38

transform take_damage_background:
    subpixel True
    alpha 0.95 xalign 0.38  yalign 0.05

transform gain_ap_background:
    subpixel True
    alpha 0.95 xalign 0.38  yalign 0.075

transform deal_damage_text:
    subpixel True
    matrixcolor TintMatrix("#fff")  yalign 0.095 alpha 1.0 xalign 0.87

transform gain_ap_text:
    subpixel True
    alpha 0.95 xalign 0.38  yalign 0.13

transform enemy_attack:
    subpixel True
    zoom 1
    alpha 0.0
    xalign 0.2 yalign 0.8
    matrixcolor BrightnessMatrix (1)
    easein 0.25 alpha 1.00 matrixcolor BrightnessMatrix (0) zoom 0.5

transform enemy_attack_animation:
    subpixel True
    xalign 1.5 yalign 0.4
    rotate 0
    matrixcolor BrightnessMatrix (0.5)
    easein 0.2 alpha 1.00 matrixcolor BrightnessMatrix (0) rotate -25 xalign 1.5  yalign 0.4

transform enemy_attack_recovery:
    subpixel True
    rotate -25
    xalign 1.5  yalign 0.4
    matrixcolor BrightnessMatrix (0.5)
    easein 0.2 alpha 1.00 matrixcolor BrightnessMatrix (0) rotate 0 xalign 1.5  yalign 0.4

transform player_attack_animation:
    subpixel True
    xalign -0.5 yalign 0.4
    rotate 0
    matrixcolor BrightnessMatrix (0.5)
    easein 0.2 alpha 1.00 matrixcolor BrightnessMatrix (0) yalign 0.4 rotate 25

transform player_attack_recovery:
    subpixel True
    xalign -0.5 yalign 0.4
    rotate 25
    matrixcolor BrightnessMatrix (0.5)
    easein 0.2 alpha 1.00 matrixcolor BrightnessMatrix (0) yalign 0.4 rotate 0

transform player_attack_animation_special:
    subpixel True
    xalign -0.5 yalign 0.4
    rotate 0
    matrixcolor TintMatrix("#fff")
    easein 0.4 alpha 1.00 matrixcolor TintMatrix("#0ff") yalign 0.4 rotate 45 xalign 3

transform player_attack_recovery_special:
    subpixel True
    xalign 0.5 yalign 0.4
    rotate 25
    matrixcolor TintMatrix("#0ff")
    easein 0.4 alpha 1.00 matrixcolor TintMatrix("#fff") yalign 0.4 rotate 0 xalign -0.5

transform player_defense:
    subpixel True
    alpha 0.0
    matrixcolor BrightnessMatrix (0.75)
    xalign 0.125 yalign 1.0
    easein 0.5 matrixcolor BrightnessMatrix (0) alpha 0.95

transform primary_attack_artificer:
    subpixel True
    zoom 1.5 alpha 1.0
    rotate -45
    xalign -0.5 yalign 0.5
    matrixcolor BrightnessMatrix (0.5)
    easein 0.3 zoom 1.00 alpha 1.00 matrixcolor BrightnessMatrix (0) xalign 0.65 rotate 45

transform primary_attack_assassin:
    subpixel True
    rotate -90
    zoom 1.5 alpha 1.0
    xalign -0.5 yalign 0.0
    matrixcolor BrightnessMatrix (0.5)
    easein 0.3 zoom 1.00 alpha 1.00 matrixcolor BrightnessMatrix (0) xalign 0.7 yalign 0.5 rotate -390

transform primary_attack_berserker:
    subpixel True
    rotate -360
    zoom 1.5 alpha 1.0
    xalign -0.5 yalign 0.5
    matrixcolor BrightnessMatrix (0.5)
    easein 0.3 zoom 1.00 alpha 1.00 matrixcolor BrightnessMatrix (0) xalign 0.75 rotate 0 yalign 0.7

transform primary_attack_gunner:
    subpixel True
    zoom 2 alpha 1.0
    yalign 0.5
    rotate 20
    xalign -0.5
    matrixcolor BrightnessMatrix (0.5)
    easein 0.3 zoom 1.25 alpha 1.00 matrixcolor BrightnessMatrix (0) rotate -90 xalign -0.2

transform primary_attack_monk:
    subpixel True
    zoom 1.5 alpha 1.0
    xalign -0.5 yalign 0.5
    matrixcolor BrightnessMatrix (0.5)
    easein 0.3 zoom 1.00 alpha 1.00 matrixcolor BrightnessMatrix (0) xalign 0.65

transform primary_attack_pirate:
    subpixel True
    zoom 1.5 alpha 1.0
    rotate -90
    xalign -0.5 yalign 0.5
    matrixcolor BrightnessMatrix (0.5)
    easein 0.3 zoom 1.00 alpha 1.00 matrixcolor BrightnessMatrix (0) xalign 0.75 rotate 45

transform smoke_bomb_throw:
    subpixel True
    alpha 3
    yalign 1
    matrixcolor BrightnessMatrix (0.5)
    easein 1 zoom 1.00 alpha 0.0 matrixcolor BrightnessMatrix (0) xalign 1.0 yalign 0.5

transform potion_use:
    subpixel True
    zoom 1.5 alpha 1.0
    xalign 0.2 yalign 0.5
    matrixcolor BrightnessMatrix (0.5)
    easein 1 zoom 1.00 alpha 0.00 matrixcolor BrightnessMatrix (0)

# The game starts here.
label start:
    scene HallOfMirrors
    show ShatteredTitleScreen at flash
    pause(1)
    hide ShatteredTitleScreen with Dissolve(1)
    pause(1)

    "You wake up in a magnificent hall adorned with stained glass murals of legendary heroes and fearsome monsters."
    show SkeletonKing_Character
    show SkeletonKing_Background
    "One in particular captures your attention—a mesmerizing depiction of the Skeleton King, seemingly alive and shifting within the glass."
    hide SkeletonKing_Character
    show SkeletonKing_Character at zoom_in
    hide SkeletonKing_Background with dissolve

    # These display lines of dialogue.
    sk "Fear not, human!"
    "You instinctively reach for your weapon, only to realize there is nothing at your side."
    sk "Here, where fate itself is woven, no blood shall be spilt."

    menu:
        "Where am I?":
            jump introduction_location_first
        "Who are you?":
            jump introduction_name_first

label introduction_name_first:
    sk "I am the Guardian of Fate, the eternal Skeleton King, once a mortal king like those you admired in tales." 
    sk "This is the Hall Of Destinies! A sanctum where souls like yours choose their path."
    "The king gestures grandly to the murals around the hall."
    sk "Each mural represents a different fate, a power for you to wield. Step forth to the one that calls to you and embrace your destiny."

    show SkeletonKing_Character at left with move
    jump class_choice

label introduction_location_first:
    sk "This is the Hall Of Destinies! A sanctum where souls like yours choose their path."
    sk "I am the Guardian of Fate, the eternal Skeleton King, once a mortal king like those you admired in tales." 
    "The king gestures grandly to the murals around the hall."
    sk "Each mural represents a different fate, a power for you to wield. Step forth to the one that calls to you and embrace your destiny."

    show SkeletonKing_Character at left with move
    jump class_choice
    
label class_choice:
    menu:
        "Artificer":
            $ player_class = "Artificer"
            jump choose_artificer
        "Assassin":
            $ player_class = "Assassin"
            jump choose_assassin
        "Berserker":
            $ player_class = "Berserker"
            jump choose_berserker
        "Gunner":
            $ player_class = "Gunner"
            jump choose_gunner
        "Monk":
            $ player_class = "Monk"
            jump choose_monk
        "Pirate":
            $ player_class = "Pirate"
            jump choose_pirate

label choose_artificer:
    show Artificer_Character at right
    show Artificer_Background at right
    pause
    hide Artificer_Character
    show Artificer_Character at flash_right
    hide Artificer_Background with Dissolve(1) 

    sk "The Artificer employs advanced technology as combat magic, using arcane-infused gadgets and mechanisms to battle foes and manipulate the environment."
    sk "He embarks on his journey with {color=#f00}100 HP{/color}, {color=#0ff}250 AP{/color}, a single HP Potion,  an AP Potion,  and a Smoke Bomb. His attacks unleash between {color=#ff0}20 and 40 damage{/color} upon his foes."
    sk "His special attack, a marvel of arcane ingenuity, deals double damage and has the chance to paralyze his enemy, nullifying their next strike."
    sk "Does that fate call out to you?"
    menu:
        "Indeed, let the gears of destiny turn!":
            hide Artificer_Character with Dissolve(0.25)
            jump fate_chosen
        "No, I shall forge a different path.":
            hide Artificer_Character with Dissolve(0.25)
            jump class_choice

label choose_assassin:
    show Assassin_Character at right
    show Assassin_Background at right
    pause
    hide Assassin_Character
    show Assassin_Character at flash_right
    hide Assassin_Background with Dissolve(1) 
    
    sk "The Assassin wields the shadows as a weapon, adept in stealth and precision strikes. Skilled in the art of silent elimination."
    sk "He begins with {color=#f00}100 HP{/color}, {color=#0ff}100 AP{/color}, one HP Potion, one AP Potion, and a generous arsenal of nine Smoke Bombs. His strikes are swift and lethal, dealing between {color=#ff0}20 and 120 damage{/color}."
    sk "His special attack, a masterful display of precision and agility, deals double damage and has a chance to steal an item from the enemy!"
    sk "Does that fate call out to you?"
    menu:
        "Yes, I shall embrace the shadows as my cloak.":
            hide Assassin_Character with Dissolve(0.25)
            jump fate_chosen
        "No, I shall walk a path bathed in light.":
            hide Assassin_Character with Dissolve(0.25)
            jump class_choice

label choose_berserker:
    show Berserker_Character at right
    show Berserker_Background at right
    pause
    hide Berserker_Character
    show Berserker_Character at flash_right
    hide Berserker_Background with Dissolve(1) 

    sk "The Berserker is a formidable warrior, channeling ferocious strength and unyielding fury in battle. This fighter thrives in the heart of conflict, using raw power and indomitable rage to overwhelm any foe."
    sk "He charges into battle with {color=#f00}250 HP{/color}, {color=#0ff}100 AP{/color}, one HP Potion, one AP Potion, and one Smoke Bomb. His ferocious strikes deal between {color=#ff0}20 and 40 damage{/color}."
    sk "His special attack, a fearsome display of raw power, deals double damage and has a chance to permanently increase his damage by 5!"
    sk "Does that fate call out to you?"
    menu:
        "Yes, I will unleash the fury within!":
            hide Berserker_Character with Dissolve(0.25)
            jump fate_chosen
        "No, I seek a more tranquil path.":
            hide Berserker_Character with Dissolve(0.25)
            jump class_choice

label choose_gunner:
    show Gunner_Character at right
    show Gunner_Background at right
    pause
    hide Gunner_Character
    show Gunner_Character at flash_right
    hide Gunner_Background with Dissolve(1) 

    sk "The Gunner is a master of ranged combat. With precision and firepower, this warrior excels at taking down enemies from a distance, utilizing both skill and cutting-edge technology."
    sk "He enters the fray with {color=#f00}100 HP{/color}, {color=#0ff}100 AP{/color}, two HP Potions, two AP Potions, and two Smoke Bombs. His unerring shots deal {color=#ff0}40 damage{/color} with pinpoint accuracy."
    sk "His special attack, a devastating burst of firepower, deals double damage and has a chance to shoot twice, dealing quadruple damage in total!"
    sk "Does that fate call out to you?"
    menu:
        "Hell yeah, lock and load!":
            hide Gunner_Character with Dissolve(0.25)
            jump fate_chosen
        "No, I prefer it up close and personal.":
            hide Gunner_Character with Dissolve(0.25)
            jump class_choice

label choose_monk:
    show Monk_Character at right
    show Monk_Background at right
    pause
    hide Monk_Character
    show Monk_Character at flash_right
    hide Monk_Background with Dissolve(1) 

    sk "The Monk channels inner peace and physical prowess, mastering martial arts and spiritual energy. With disciplined strikes, this warrior uses harmony and strength to overcome challenges."
    sk "He commences his journey with {color=#f00}175 HP{/color}, {color=#0ff}175 AP{/color}, one HP Potion, one AP Potion, and one Smoke Bomb. His strikes deal between {color=#ff0}20 and 40 damage{/color}."
    sk "His special attack, a harmonious blend of power and tranquility, deals double damage and has a chance to heal the monk equal to the damage dealt!"
    sk "Does that fate call out to you?"
    menu:
        "Yes, I shall walk the path of enlightenment.":
            hide Monk_Character with Dissolve(0.25)
            jump fate_chosen
        "No, my journey lies elsewhere.":
            hide Monk_Character with Dissolve(0.25)
            jump class_choice

label choose_pirate:
    show Pirate_Character at right
    show Pirate_Background at right
    pause
    hide Pirate_Character
    show Pirate_Character at flash_right
    hide Pirate_Background with Dissolve(1) 

    sk "The Pirate is a swashbuckling adventurer, skilled in swordplay and naval tactics. With a fearless spirit and a lust for treasure, this rogue navigates the seas, wielding both cutlass and an anchor with deadly precision."
    sk "He sets sail with {color=#f00}100 HP{/color}, {color=#0ff}100 AP{/color}, five HP Potions, five AP Potions, and five Smoke Bombs. His attacks deal between {color=#ff0}20 and 40 damage{/color}."
    sk "His special attack, a ruthless display of cunning and might, deals double damage and has a chance to daze his enemy, permanently lowering its damage!"
    sk "Does that fate call out to you?"
    menu:
        "Aye, I shall sail the seven seas!":
            hide Pirate_Character with Dissolve(0.25)
            jump fate_chosen
        "Nay, my course lies elsewhere.":
            hide Pirate_Character with Dissolve(0.25)
            jump class_choice

label fate_chosen:
    show SkeletonKing_Character at center with move

    if player_class == "Artificer":
        sk "Ingenious Artificer! Forge ahead, where gears and alchemical contraptions create a symphony of progress. The world is your workshop."
    elif player_class == "Assassin":
        sk "Shadow-walker, the night itself parts before you. Embrace the darkness that binds your fate."
    elif player_class == "Berserker":
        sk "Mighty Berserker, let your battle cry shatter the skies! Wield your rage as a weapon, and let no foe stand unyielded before your unstoppable fury."
    elif player_class == "Gunner":
        sk "Marksman supreme, Gunner, your aim is as true as your resolve. With each pull of the trigger, enforce your will upon the battlefield."
    elif player_class == "Monk":
        sk "Enlightened Monk, your path is one of harmony and devastating precision. Balance the energies of body and spirit to break mountains with your fists."
    elif player_class == "Pirate":
        sk "Captain of the seas, Pirate, your heart beats to the rhythm of the ocean's roiling waves. Chart your course for treasure and adventure beyond the horizon."

    jump ask_name

label ask_name:
    sk "Before you step forth to claim your destiny, tell me, traveler of the mortal realms—what name do you bear? Let it echo through the Hall of Destinies, a herald of the saga you are about to weave."

    $ player_name = renpy.input("My name is... ")
    $ player_name = player_name.strip()  # Remove any extra whitespace from the name

    if player_name == "":
        $ player_name = "Nameless"

    sk "Ah, so it is spoken. Welcome, [player_name] the [player_class]. Your journey begins now."

    hide SkeletonKing_Character with Dissolve(0.25)

    $ Player_HP_Sprite = "HP_100"
    $ Player_AP_Sprite = "AP_100"

    if player_class == "Artificer":
        $ player_max_action_points = 250
        $ player_action_points = 250
        $ Player_Sprite = "Artificer_Character"
        $ Primary_Attack = "Artificer_Primary_Attack"

    elif player_class == "Assassin":
        $ player_max_damage = player_max_damage * 3
        $ smoke_bombs_amount = 9
        $ Player_Sprite = "Assassin_Character"
        $ Primary_Attack = "Assassin_Primary_Attack"

    elif player_class == "Berserker":
        $ player_health = 250
        $ player_max_health = 250
        $ Player_Sprite = "Berserker_Character"
        $ Primary_Attack = "Berserker_Primary_Attack"

    elif player_class == "Gunner":
        $ player_min_damage = player_max_damage
        $ health_potions_amount = 2
        $ action_points_potions_amount = 2
        $ smoke_bombs_amount = 2
        $ Player_Sprite = "Gunner_Character"
        $ Primary_Attack = "Gunner_Primary_Attack"

    elif player_class == "Monk":
        $ player_max_action_points = 175
        $ player_action_points = 175
        $ player_health = 175
        $ player_max_health = 175
        $ Player_Sprite = "Monk_Character"
        $ Primary_Attack = "Monk_Primary_Attack"

    elif player_class == "Pirate":
        $ health_potions_amount = 5
        $ action_points_potions_amount = 5
        $ smoke_bombs_amount = 5
        $ Player_Sprite = "Pirate_Character"
        $ Primary_Attack = "Pirate_Primary_Attack"

    jump advance_area

label advance_area:
    "As you advance, the world around you begins to splinter like delicate glass, each step echoing as reality itself shatters and shifts into breathtaking new forms." 
    jump randomize_map

label randomize_map:
    $ chosen_map = renpy.random.choice(["AbandonedCastle", "AncientRuins", "CityInTheClouds", "CrystalCaverns", 
        "DarkForest", "EnchantedGarden", "FloatingIslands", "GrassyFields", "HauntedMansion", "HiddenLaboratory", "LavaFields", 
        "MountainRange", "PirateCove", "ScorchingDesert", "SnowyMountains", 
        "SwampyMarsh"])
    if chosen_map == last_map:
        jump randomize_map #to prevent the same map appearing twice in a row

    $ last_map = chosen_map
    
if chosen_map == "AbandonedCastle":
    scene AbandonedCastle
    "Shrouded in the mist of time, the Abandoned Castle stands as a silent sentinel, its crumbling towers and shadowed gateways echoing with the whispers of ancient secrets and lost souls."

elif chosen_map == "AncientRuins":
    scene AncientRuins
    "Amidst the whispering winds, the Ancient Ruins rise majestically, a testament to forgotten civilizations, their intricate stone carvings and faded murals hinting at a once-glorious past."

elif chosen_map == "CityInTheClouds":
    scene CityInTheClouds
    "Floating serenely above the world, the City in the Clouds dazzles with its radiant towers of light and air, a haven untouched by earthly woes, suspended in the boundless sky."

elif chosen_map == "CrystalCaverns":
    scene CrystalCaverns
    "The Crystal Caverns shimmer with an ethereal glow, their jagged crystal formations casting prismatic dances of light across their mysterious, shadow-filled depths."

elif chosen_map == "DarkForest":
    scene DarkForest
    "In the heart of the Dark Forest, ancient trees stand like silent watchers, their thick canopies a permanent twilight where secrets and creatures lurk in the haunting stillness."

elif chosen_map == "EnchantedGarden":
    scene EnchantedGarden
    "The Enchanted Garden blooms in eternal splendor, its vibrant flowers and magical fauna radiating an aura of enchantment, a sanctuary of peace in a world of chaos."

elif chosen_map == "FloatingIslands":
    scene FloatingIslands
    "Suspended in the open sky, the Floating Islands drift gently, their verdant landscapes and cascading waterfalls a dreamlike vision, floating in peaceful isolation from the world below."

elif chosen_map == "GrassyFields":
    scene GrassyFields
    "The Grassy Fields stretch endlessly, a sea of green under the open sky, the soft rustling of the grass whispering tales of serenity and the simple joys of nature."

elif chosen_map == "HauntedMansion":
    scene HauntedMansion
    "Under the moon's ghostly glow, the Haunted Mansion looms forebodingly, its dark windows and eerie silence promising tales of terror and restless spirits lurking in its desolate halls."

elif chosen_map == "HiddenLaboratory":
    scene HiddenLaboratory
    "Concealed from the prying eyes of the world, the Hidden Laboratory buzzes with the electric hum of invention, a nexus of brilliance and madness where science breaks the bonds of reality."

elif chosen_map == "LavaFields":
    scene LavaFields
    "The Lava Fields roar with the fierce power of the earth, their molten rivers and explosive geysers a blazing spectacle of nature's untamed strength and fiery heart."

elif chosen_map == "MountainRange":
    scene MountainRange
    "The Mountain Range rises rugged and majestic, its towering peaks veiled in clouds, a formidable barrier standing sentinel over the secrets of the ages."

elif chosen_map == "PirateCove":
    scene PirateCove
    "Tucked away in a secluded bay, Pirate Cove whispers of adventure and old legends, its sandy shores and hidden treasures a haven for those who live by the wind and the wave."

elif chosen_map == "ScorchingDesert":
    scene ScorchingDesert
    "The Scorching Desert stretches vast and merciless under the blazing sun, its endless sands a shimmering ocean where mirages play tricks and only the hardiest survive."

elif chosen_map == "SnowyMountains":
    scene SnowyMountains
    "Enveloped in a blanket of pristine white, the Snowy Mountains stand silent in their icy splendor, a world where the air is crisp and the stillness is broken only by the crunch of snow."

elif chosen_map == "SwampyMarsh":
    scene SwampyMarsh
    "The Swampy Marsh cloaks its secrets in a heavy mist, its murky waters and soggy earth home to strange creatures and plants, a mysterious labyrinth where land and water blur."

    jump pre_combat
  
label pre_combat:
    "You stand here, captivated by the sheer splendor of the surroundings."
    "Until Suddenly..."
    jump randomize_enemy

label randomize_enemy:
    $ chosen_enemy = renpy.random.choice(["Cthulhu", "Demon", "Gargoyle", "Wolf", "Slime", "Bandit"])
    $ Enemy_Health_Sprite = "Enemy_100"

    if chosen_enemy == last_enemy:
        jump randomize_enemy #to prevent the same enemy appearing twice in a row

    $ last_enemy = chosen_enemy

if chosen_enemy == "Bandit":
    if enemies_defeated > 0 and enemies_defeated % enemies_required_for_boss == 0:
        $ Enemy_Sprite = "Bandit_Boss"
        $ Enemy_Attack = "Boss_Attack"
    else:
        $ Enemy_Sprite = "Bandit_Character"
        $ Enemy_Attack = "Bandit_Attack"
    show expression Enemy_Sprite at enemy_appear
    $ negotiate_dc = 10
    $ run_dc = 10
    "A Bandit leaps forward, his blades slicing through the air as he charges directly at you, his fierce cry echoing in the still air."

elif chosen_enemy == "Cthulhu":
    if enemies_defeated > 0 and enemies_defeated % enemies_required_for_boss == 0:
        $ Enemy_Sprite = "Cthulhu_Boss"
        $ Enemy_Attack = "Boss_Attack"
    else:
        $ Enemy_Sprite = "Cthulhu_Character"
        $ Enemy_Attack = "Cthulhu_Attack"
    show expression Enemy_Sprite at enemy_appear
    $ negotiate_dc = 21
    $ run_dc = 21
    "The ground vibrates intensely as Cthulhu emerges, towering over you with its immense form and writhing tentacles, overwhelming your senses."

elif chosen_enemy == "Demon":
    if enemies_defeated > 0 and enemies_defeated % enemies_required_for_boss == 0:
        $ Enemy_Sprite = "Demon_Boss"
        $ Enemy_Attack = "Boss_Attack"
    else:
        $ Enemy_Sprite = "Demon_Character"
        $ Enemy_Attack = "Demon_Attack"
    show expression Enemy_Sprite at enemy_appear
    $ negotiate_dc = 10
    $ run_dc = 15
    "A Demon appears before you, its wings unfurling dramatically and its terrifying visage filling your field of vision."

elif chosen_enemy == "Gargoyle":
    if enemies_defeated > 0 and enemies_defeated % enemies_required_for_boss == 0:
        $ Enemy_Sprite = "Gargoyle_Boss"
        $ Enemy_Attack = "Boss_Attack"
    else:
        $ Enemy_Sprite = "Gargoyle_Character"
        $ Enemy_Attack = "Gargoyle_Attack"
    show expression Enemy_Sprite at enemy_appear
    $ negotiate_dc = 15
    $ run_dc = 15
    "A nearby Gargoyle transforms from statue to predator, launching a sudden attack that leaves you barely any time to react."

elif chosen_enemy == "Wolf":
    if enemies_defeated > 0 and enemies_defeated % enemies_required_for_boss == 0:
        $ Enemy_Sprite = "Wolf_Boss"
        $ Enemy_Attack = "Boss_Attack"
    else:
        $ Enemy_Sprite = "Wolf_Character"
        $ Enemy_Attack = "Wolf_Attack"
    show expression Enemy_Sprite at enemy_appear
    $ negotiate_dc = 21
    $ run_dc = 15
    "A Wolf darts out of nowhere, its eyes locked onto yours as it charges."

elif chosen_enemy == "Slime":
    if enemies_defeated > 0 and enemies_defeated % enemies_required_for_boss == 0:
        $ Enemy_Sprite = "Slime_Boss"
        $ Enemy_Attack = "Boss_Attack"
    else:
        $ Enemy_Sprite = "Slime_Character"
        $ Enemy_Attack = "Slime_Attack"
    show expression Enemy_Sprite at enemy_appear
    $ negotiate_dc = 21
    $ run_dc = 5
    "A Slime emerges right in front of you, quickly expanding across the ground, its sudden movement catching you off guard as it attempts to engulf you."

$ enemy_hp = base_enemy_hp + enemies_defeated * enemy_hp_increase_per_level
$ max_enemy_hp = enemy_hp
$ enemy_min_damage = base_enemy_min_damage + enemies_defeated * enemy_damage_inrease_per_level
$ enemy_max_damage = base_enemy_max_damage + enemies_defeated * enemy_damage_inrease_per_level

if enemies_defeated > 0 and enemies_defeated % enemies_required_for_boss == 0:
    $ enemy_hp = enemy_hp * enemy_boss_stats_multiplier
    $ max_enemy_hp = max_enemy_hp * enemy_boss_stats_multiplier
    $ enemy_min_damage = enemy_min_damage * enemy_boss_stats_multiplier
    $ enemy_max_damage = enemy_max_damage * enemy_boss_stats_multiplier
    "You catch the subtle, chilling scent of blood on the air... a foreboding sense grips you, warning that this foe will not fall without a fierce struggle."

jump pre_combat_menu

label pre_combat_menu:
    $ player_health = player_max_health
    menu:
        "Fight!":
            jump pre_fight_animation
        "Negotiate":
            jump pre_combat_negotiate
        "Run":
            jump pre_combat_run

label pre_combat_negotiate:

    $ run_rng = renpy.random.randint(0, 20);

    if run_rng > run_dc:
        "With a surge of eloquence, you sway the heart of your foe, who finds no honor in needless battle. Yet, remember, the path of peace garners no spoils of war."
        hide expression Enemy_Sprite with Dissolve(0.25)
        jump advance_area
    else:
        "Alas, despite your efforts, the adversary remains unmoved, their resolve unshaken. Ready your arms, for battle is now inevitable!"

    jump pre_fight_animation

return

label pre_combat_run:

    $ run_rng = renpy.random.randint(0, 20);

    if run_rng > run_dc:
        "With a rush of wind and a blur of the world, you successfully elude your foe. Yet, bear in mind, retreat bears no laurels nor glory."
        show expression Enemy_Sprite at enemy_run_away
        pause(0.25)
        hide expression Enemy_Sprite with Dissolve(0.25)
        jump advance_area
    else:
        "Alas, your escape is thwarted by fate's cruel hand! Steel yourself, for combat is now your only recourse!"

    jump pre_fight_animation
return

label pre_fight_animation:
    show expression Player_Sprite at player_ready_to_fight
    show expression Enemy_Sprite at right with move
    $ update_player_hp_sprite(player_health, player_max_health)
    show expression Player_HP_Sprite at player_hp_show
    $ update_player_ap_sprite(player_action_points, player_max_action_points)
    show expression Player_AP_Sprite at player_ap_show
    $ update_enemy_health_sprite(enemy_hp, max_enemy_hp)
    show expression Enemy_Health_Sprite at enemy_hp_show
    pause(0.25)
    jump fight_choice

label fight_choice:
    menu:
        "Attack":
            jump player_attack
        "Defend":
            jump player_defense
        "Special" if player_action_points > 24:
            $ player_action_points = player_action_points - 25
            $ update_player_ap_sprite(player_action_points, player_max_action_points)
            show expression Player_AP_Sprite at player_ap_show
            jump special_attack
        "Item":
            jump use_item
        "Status":
            jump combat_status
        "Guidance":
            jump tutorial_fight
    pause

label use_item:
        menu:
            "HP Potion x[health_potions_amount]" if health_potions_amount > 0:
                show HP_Potion at potion_use
                $ player_health = player_max_health
                $ update_player_hp_sprite(player_health, player_max_health)
                $ health_potions_amount = health_potions_amount - 1
                show expression Player_HP_Sprite at player_hp_show
                "You quaff the HP potion, feeling its potent elixir coursing through your veins. Instantly, your strength returns in full, every wound healing as you stand revitalized and ready for battle."
                jump fight_choice
            "AP Potion x[action_points_potions_amount]" if action_points_potions_amount > 0:
                show AP_Potion at potion_use
                $ player_action_points = player_max_action_points
                show expression Player_AP_Sprite at player_ap_show
                $ action_points_potions_amount = action_points_potions_amount - 1
                $ update_player_ap_sprite(player_action_points, player_max_action_points)
                show expression Player_AP_Sprite at player_ap_show
                "You down the AP potion, its energy surging through you like a lightning bolt. In an instant, your action points are fully restored, empowering you to unleash your full potential once more."
                jump fight_choice
            "Smoke Bomb x[smoke_bombs_amount]" if smoke_bombs_amount > 0:
                show Smoke_Bomb at smoke_bomb_throw
                "With a swift motion, you hurled the smoke bomb into the fray, its billowing cloud enveloping your enemy. Seizing the moment, you vanished into the shadows, escaping unseen into the night."
                hide expression Enemy_Sprite with Dissolve(0.25)
                hide expression Enemy_Health_Sprite
                hide expression Player_Sprite
                hide expression Player_HP_Sprite
                hide expression Player_AP_Sprite
                $ smoke_bombs_amount = smoke_bombs_amount - 1
                hide text
                jump advance_area
            "Nevermind...":
                jump fight_choice

label combat_status:
    "Current HP: {color=#f00}[player_health]/[player_max_health]{/color}  \nCurrent AP: {color=#0ff}[player_action_points]/[player_max_action_points]{/color} \nDamage: {color=#ff0} between [player_min_damage] and [player_max_damage] {/color} \nSpecial Attack: {color=#0ff}[player_special_damage_multiplier]x{/color} damage, with {color=#0ff}[player_special_bonus_effect_chance] percent{/color} chance for effect."
    "Enemy HP: {color=#f00}[enemy_hp]/[max_enemy_hp]{/color}"
    jump fight_choice

label level_up_choice:
    menu:
        "Increase Damage":
            $ player_min_damage = player_min_damage + damage_increase_per_level
            $ player_max_damage = player_max_damage + damage_increase_per_level
            "Damage increased from {color=#ff0} between [player_min_damage - damage_increase_per_level] and [player_max_damage - damage_increase_per_level] {/color} to {color=#ff0} between [player_min_damage] and [player_max_damage] {/color}"
            jump advance_area
        "Increase HP":
            $ player_max_health = player_max_health + health_increase_per_level
            $ player_health = player_health + health_increase_per_level
            "Max HP increased from {color=#f00}[player_max_health - health_increase_per_level]{/color} to {color=#f00}[player_max_health]{/color} "
            jump advance_area
        "Increase AP":
            $ player_max_action_points = player_max_action_points + action_points_per_level
            $ player_action_points = player_action_points + action_points_per_level
            "Max AP increased from {color=#0ff}[player_max_action_points - action_points_per_level]{/color} to {color=#0ff}[player_max_action_points]{/color} "
            jump advance_area
        "Improve Special Attack":
            $ player_special_bonus_effect_chance = player_special_bonus_effect_chance + special_bonus_effect_chance_per_level
            $ player_special_damage_multiplier = player_special_damage_multiplier + special_damage_multiplier_increase_per_level
            $ player_special_damage_multiplier = float("{:.1f}".format(player_special_damage_multiplier))
            "Increased special attack damage multiplier to {color=#0ff}x[player_special_damage_multiplier]{/color} and special attack effect chance increased from {color=#0ff}[player_special_bonus_effect_chance - special_bonus_effect_chance_per_level] percent{/color} to {color=#0ff}[player_special_bonus_effect_chance] percent{/color}. "
            jump advance_area
        "Status":
            jump player_status
        "Guidance":
            jump level_up_tutorial
    pause

label tutorial_level_up:
    hide expression Player_Sprite with Dissolve(0.25)
    show SkeletonKing_Character at enemy_appear
    sk "Ah, I see you have vanquished a foe, well done!"
    sk "Your reward shall be great, for you may choose between enhancing your damage, health points, action points, or improving your special attack."
    sk "Increase Damage - Boosts your minimum and maximum damage by 10 each."
    sk "Increase HP - Raises your maximum HP by 50 and heals you for the same amount."
    sk "Increase AP - Augments your maximum AP by 50 and restores it for the same amount."
    sk "Improve special attack - Increases your special attack's damage and special effect chance by 10% each."
    sk "Choose wisely, and don't hesitate to call upon me if you ever forget."
    hide SkeletonKing_Character with Dissolve(0.25)
    show expression Player_Sprite at player_ready_to_fight
    pause(0.25)
    jump level_up_choice

label player_status:
    "Max HP: {color=#f00}[player_max_health]{/color}, Max AP: {color=#0ff}[player_max_action_points]{/color}, Damage: {color=#ff0}between [player_min_damage] and [player_max_damage]{/color}. \nSpecial: {color=#0ff}[player_special_damage_multiplier]x{/color} multiplier with {color=#0ff}[player_special_bonus_effect_chance] percent {/color} chance for special effect."
    pause(0.25)
    jump level_up_choice

label tutorial_fight:
    hide expression Player_Sprite with Dissolve(0.25)
    hide expression Enemy_Sprite with Dissolve(0.25)
    show SkeletonKing_Character at enemy_appear
    sk "Beyond the confines of my Hall, my influence wanes, young warrior. In the throes of combat, you must rely upon your own strength and cunning. However, allow me to enlighten you on the mastery of your abilities."
    sk "Attack - Your primary weapon against darkness. This basic assault demands no action points, yet stands as your staunchest ally in vanquishing foes."
    show Defend at showcase_thing_left
    sk "Defend - Forge your resilience. Halve the blow from your adversary's next strike and reclaim action points equal to the damage thwarted."
    hide Defend
    sk "Special - a testament to your chosen fate, costs {color=#0ff}[player_special_attack_cost] AP{/color}. With each strike, there lies a chance for a unique and powerful effect, determined by the destiny you embrace."
    sk "Item - From my realm, I bestow upon you initial armaments; the fallen may offer more. Utilize them wisely."
    show HP_Potion at showcase_thing_left
    show AP_Potion at showcase_thing_right
    sk "HP Potions to fully mend your wounds, AP Potions to renew your action points."
    hide HP_Potion
    hide AP_Potion
    show Smoke_Bomb at showcase_thing_right
    sk "And Smoke Bombs to flee from battle, as your last resort."
    hide Smoke_Bomb
    sk "Should the haze of battle cloud your memory, summon my guidance anew."
    sk "Farewell, may your path be illuminated by the stars of fortune."
    hide SkeletonKing_Character with Dissolve(0.25)
    show expression Player_Sprite at player_ready_to_fight
    show expression Enemy_Sprite at enemy_ready_to_fight
    pause(0.25)
    jump fight_choice

label player_attack:
    if player_class == "Artificer":
        show expression Primary_Attack at primary_attack_artificer
    if player_class == "Assassin":
        show expression Primary_Attack at primary_attack_assassin
    if player_class == "Berserker":
        show expression Primary_Attack at primary_attack_berserker
    if player_class == "Gunner":
        show expression Primary_Attack at primary_attack_gunner
    if player_class == "Monk":
        show expression Primary_Attack at primary_attack_monk
    if player_class == "Pirate":
        show expression Primary_Attack at primary_attack_pirate

    show expression Player_Sprite at player_attack_animation
    show expression Player_Sprite at player_attack_recovery
    pause (0.25)

    $ player_damage = renpy.random.randint(player_min_damage, player_max_damage)
    $ enemy_hp = enemy_hp - player_damage
    $ update_enemy_health_sprite(enemy_hp, max_enemy_hp)
    show expression Enemy_Health_Sprite at take_damage

    if enemy_hp < 1:
        show expression Enemy_Sprite at take_damage
        hide expression Primary_Attack with Dissolve (0.5)
        show expression Enemy_Sprite at enemy_dead
        hide expression Enemy_Sprite with Dissolve(2)
        jump enemy_defeated
    else:
        show expression Enemy_Sprite at take_damage
        hide expression Primary_Attack with Dissolve(0.5)
        jump enemy_attack
    

label special_attack:
    if player_class == "Artificer":
        show expression Primary_Attack at primary_attack_artificer
    if player_class == "Assassin":
        show expression Primary_Attack at primary_attack_assassin
    if player_class == "Berserker":
        show expression Primary_Attack at primary_attack_berserker
    if player_class == "Gunner":
        show expression Primary_Attack at primary_attack_gunner
    if player_class == "Monk":
        show expression Primary_Attack at primary_attack_monk
    if player_class == "Pirate":
        show expression Primary_Attack at primary_attack_pirate

    show expression Player_Sprite at player_attack_animation_special
    show expression Player_Sprite at player_attack_recovery_special
    pause (0.25)

    $ player_damage = renpy.random.randint(player_min_damage, player_max_damage) * player_special_damage_multiplier
    $ enemy_hp = enemy_hp - player_damage
    $ update_enemy_health_sprite(enemy_hp, max_enemy_hp)
    show expression Enemy_Health_Sprite at take_damage

    $ special_effect_roll = renpy.random.randint(0, 100);
    if special_effect_roll < player_special_bonus_effect_chance:
        "Against all odds, your special move has unleashed its hidden power!"
        if player_class == "Artificer":
            "The enemy is paralyzed, its next attack nullified by your powerful strike!"
            $ is_enemy_paralyzed = True

        if player_class == "Assassin":
            "With deft precision, you pilfer an item from your enemy, claiming it as your own."
            $ random_number_roll = renpy.random.randint(1, 3)
            if random_number_roll == 1:
                "Your reward... an HP potion!"
                $ health_potions_amount = health_potions_amount + 1
            if random_number_roll == 2:
                "Your reward... an AP potion!"
                $ action_points_potions_amount = action_points_potions_amount + 1
            if random_number_roll == 3:
                "Your reward... a Smoke Bomb!"
                $ smoke_bombs_amount = smoke_bombs_amount + 1

        if player_class == "Berserker":
            "Your strength surges as you permanently ascend to new heights of power! Your damage has increased from {color=#ff0}between [player_min_damage] and [player_max_damage]{/color} to {color=#ff0}between [player_min_damage + 5] and [player_max_damage + 5]{/color}"
            $ player_min_damage = player_min_damage + 5
            $ player_max_damage = player_max_damage + 5

        if player_class == "Gunner":
            "You unleash a rapid burst from your firearm—Double Shot!"
            show expression Primary_Attack at primary_attack_gunner
            $ enemy_hp = enemy_hp - player_damage
            $ update_enemy_health_sprite(enemy_hp, max_enemy_hp)
            show expression Enemy_Health_Sprite at take_damage

        if player_class == "Monk":
            "Your strike siphons life force from your foe, healing your wounds with the blow!"
            $ player_health = player_health + player_damage
            if player_health > player_max_health:
                $ player_health = player_max_health
            $ update_player_hp_sprite(player_health, player_max_health)
            show expression Player_HP_Sprite at player_hp_show

        if player_class == "Pirate":
            "The enemy staggers, dazed and disoriented, its attack power permanently diminished!"
            $ enemy_min_damage = enemy_min_damage - 10
            $ enemy_max_damage = enemy_max_damage - 10

    if enemy_hp < 1:
        show expression Enemy_Sprite at take_damage
        hide expression Primary_Attack with Dissolve (0.5)
        show expression Enemy_Sprite at enemy_dead
        hide expression Enemy_Sprite with Dissolve(2)
        jump enemy_defeated
    elif is_enemy_paralyzed:
        $ is_enemy_paralyzed = False
        hide expression Primary_Attack with Dissolve (0.5)
        jump fight_choice
    else:
        show expression Enemy_Sprite at take_damage
        hide expression Primary_Attack with Dissolve(0.5)
        jump enemy_attack

label enemy_defeated:
    $ enemies_defeated = enemies_defeated + 1
    hide expression Enemy_Health_Sprite with Dissolve (0.25)
    "With a final strike, you vanquish your enemy, standing victorious over their fallen form."
    $ random_number_roll = renpy.random.randint(0, 5)
    if random_number_roll == 1:
        "Amidst the spoils of battle, you discover a precious HP Potion!"
        $ health_potions_amount = health_potions_amount + 1
    if random_number_roll == 2:
        "Amidst the spoils of battle, you discover a powerful AP Potion!"
        $ action_points_potions_amount = action_points_potions_amount + 1
    if random_number_roll == 3:
        "Amidst the spoils of battle, you discover a tactical Smoke Bomb!"
        $ smoke_bombs_amount = smoke_bombs_amount + 1
    jump level_up_choice

label enemy_attack:
    show expression Enemy_Attack at enemy_attack
    show expression Enemy_Sprite at enemy_attack_animation
    show expression Enemy_Sprite at enemy_attack_recovery

    $ enemy_damage = renpy.random.randint(enemy_min_damage, enemy_max_damage)

    if is_player_defending == True:
        $ enemy_damage = int(enemy_damage / 2)

    show expression Player_Sprite at take_damage

    if is_player_defending == True:
        show Defend at player_defense
        $ player_action_points = player_action_points + enemy_damage
        if player_action_points > player_max_action_points:
            $ player_action_points = player_max_action_points
        $ update_player_ap_sprite(player_action_points, player_max_action_points)
        show expression Player_AP_Sprite at player_ap_show

    $ player_health = player_health - enemy_damage
    
    $ update_player_hp_sprite(player_health, player_max_health)
    show expression Player_HP_Sprite at take_damage
           

    pause (0.25)
    #show text "-"+str(enemy_damage) at take_damage_text
    hide expression Enemy_Attack with Dissolve(0.5)

    if is_player_defending == True:
        #show text str(enemy_damage) at gain_ap_text
        hide Defend with Dissolve(0.5)
    hide text
    $ is_player_defending = False

    if player_health < 1:
        show expression Player_Sprite at enemy_dead
        pause(2)
        hide expression Player_HP_Sprite with Dissolve(0.25)
        hide expression Player_AP_Sprite with Dissolve(0.25)
        jump player_dead

    jump fight_choice

label player_dead:
    scene Graveyard
    "Fate has turned against you..."
    show SkeletonKing_Character at zoom_in
    sk "You have fallen in battle."
    sk "In your valiant struggle, you vanquished [enemies_defeated] foes."
    sk "May destiny favor you in your next endeavor."
    $ MainMenu(confirm=False)()

label player_defense:
    show Defend at player_defense
    $ is_player_defending = True
    jump enemy_attack
    

return